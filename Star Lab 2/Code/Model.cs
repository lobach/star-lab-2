﻿using System.Collections.Generic;
using StarLab2.Code.UI.Memento;
using StarLab2.Code.Visual;

namespace StarLab2.Code;

public class Model : IModel
{
    public List<VisualCurve> Curves { get; private set; }

    public Model() => 
        Curves = new List<VisualCurve>();

    private Model(List<VisualCurve> curves) =>
        Curves = curves;

    public void AddCurve(VisualCurve curve) =>
        Curves.Add(curve);

    public void RemoveCurve(VisualCurve curve) =>
        Curves.Remove(curve);

    public Model Clone()
    {
        var curves = new List<VisualCurve>(Curves.Count);
        foreach (var curve in Curves)
            curves.Add((curve.Clone() as VisualCurve)!);

        return new Model(curves);
    }
}