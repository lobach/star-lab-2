﻿namespace StarLab2.Code;

public interface ICloneable<T>
{
    public T Clone();
}