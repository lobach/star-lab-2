﻿namespace StarLab2.Code.Geometry;

public class Line : ACurve
{
    public Line(IPoint a, IPoint b) : base(a, b)
    {
    }

    public override IPoint GetPoint(double t)
        => new Point(Evaluate(A.X, B.X, t), Evaluate(A.Y, B.Y, t));

    public override ICurve Clone() => 
        new Line(A.Clone(), B.Clone());

    private double Evaluate(double a, double b, double t) =>
        (1 - t) * a + t * b;
}