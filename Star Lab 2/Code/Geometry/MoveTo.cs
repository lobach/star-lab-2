﻿using System.Collections;
using System.Collections.Generic;

namespace StarLab2.Code.Geometry;

public class MoveTo : ICurve
{
    private readonly ICurve _curve;
    private readonly IPoint _point;

    public MoveTo(ICurve curve, IPoint point)
    {
        _curve = curve;
        _point = point;
    }

    public void Accept(IVisitor visitor) =>
        visitor.VisitCurve(this);

    public IPoint GetPoint(double t)
    {
        var point = _curve.GetPoint(t);
        point.X += _point.X;
        point.Y += _point.Y;
        return point;
    }

    public ICurve Clone() => 
        new MoveTo(_curve.Clone(), _point.Clone());

    public IEnumerator<ICurve> GetEnumerator() => 
        null;

    IEnumerator IEnumerable.GetEnumerator() => 
        GetEnumerator();
}