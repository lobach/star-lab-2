﻿namespace StarLab2.Code.Geometry;

public class Bezier : ACurve
{
    protected readonly IPoint C;
    protected readonly IPoint D;

    public Bezier(IPoint a, IPoint b, IPoint c, IPoint d) : base(a, b)
    {
        C = c;
        D = d;
    }

    public override IPoint GetPoint(double t) => 
        new Point(Evaluate(A.X, B.X, C.X, D.X, t), Evaluate(A.Y, B.Y, C.Y, D.Y, t));

    public override ICurve Clone() => 
        new Bezier(A.Clone(), B.Clone(), C.Clone(), D.Clone());

    private double Evaluate(double a, double b, double c, double d, double t) =>
        System.Math.Pow(1 - t, 3) * a
        + 3 * t * System.Math.Pow(1 - t, 2) * c
        + 3 * System.Math.Pow(t, 2) * (1 - t) * d
        + System.Math.Pow(t, 3) * b;
}