﻿using System.Collections.Generic;

namespace StarLab2.Code.Geometry;

public interface ICurve : IVisitable, IEnumerable<ICurve>, ICloneable<ICurve>
{
    public IPoint GetPoint(double t);
}