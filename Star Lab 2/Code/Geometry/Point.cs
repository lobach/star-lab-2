﻿namespace StarLab2.Code.Geometry;

public class Point : IPoint
{
    public double X { get; set; }
    public double Y { get; set; }

    public Point(double x, double y)
    {
        X = x;
        Y = y;
    }

    public IPoint Clone() => 
        new Point(X, Y);
}