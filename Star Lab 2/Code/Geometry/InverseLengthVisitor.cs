﻿namespace StarLab2.Code.Geometry;

public class InverseLengthVisitor : Visitor<double>
{
    private readonly double _l;

    public InverseLengthVisitor(double l) =>
        _l = l;

    protected override double CalcResult(ICurve curve) =>
        curve.CalcLenProcessor(data => data.TotalDistance >= _l).CurrentStep;
}