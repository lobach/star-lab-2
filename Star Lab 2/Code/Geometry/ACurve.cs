﻿using System.Collections;
using System.Collections.Generic;

namespace StarLab2.Code.Geometry;

public abstract class ACurve : ICurve
{
    protected readonly IPoint A;
    protected readonly IPoint B;

    protected ACurve(IPoint a, IPoint b)
    {
        A = a;
        B = b;
    }

    public abstract IPoint GetPoint(double t);

    public void Accept(IVisitor visitor) => 
        visitor.VisitCurve(this);

    public IEnumerator<ICurve> GetEnumerator() => 
        null;

    IEnumerator IEnumerable.GetEnumerator() => 
        GetEnumerator();

    public abstract ICurve Clone();
}