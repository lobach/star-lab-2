﻿namespace StarLab2.Code.Geometry;

public struct CalcData
{
    public double CurrentStep { get; set; }
    public double TotalDistance { get; set; }
    public IPoint Point { get; set; }
}