﻿using System;

namespace StarLab2.Code.Geometry;

public static class Helper
{
    public static CalcData CalcLenProcessor(this ICurve curve, Predicate<CalcData> checkComplete)
    {
        var calcData = new CalcData();
        const int stepsCount = 1000;
        const double step = 1.0 / stepsCount;

        IPoint prevPoint = null;
        for (var i = 0; i < stepsCount; i++)
        {
            var point = curve.GetPoint(calcData.CurrentStep);
            calcData.Point = point;
            if (prevPoint != null)
                calcData.TotalDistance += Math.Distance(prevPoint, point);

            if (checkComplete(calcData))
                break;

            prevPoint = point;
            calcData.CurrentStep += step;
        }

        return calcData;
    }
}