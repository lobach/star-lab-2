﻿using System.Collections;
using System.Collections.Generic;

namespace StarLab2.Code.Geometry;

public class Chain : ICurve
{
    private readonly List<ICurve> _curves;

    public Chain(params ICurve[] curves) =>
        _curves = new List<ICurve>(curves);

    private Chain(List<ICurve> curves) =>
        _curves = curves;

    public void AddCurve(ICurve curve) =>
        _curves.Add(curve);

    public void Accept(IVisitor visitor) =>
        visitor.VisitCurve(this);

    public IPoint GetPoint(double t)
    {
        var segmentSize = 1.0 / _curves.Count;
        var segment = System.Math.Clamp((int) (t / segmentSize), 0, _curves.Count - 1);
        var lerpK = (t - segmentSize * segment) * _curves.Count;
        return _curves[segment].GetPoint(lerpK);
    }

    public IEnumerator<ICurve> GetEnumerator() =>
        new Enumerator(_curves);

    IEnumerator IEnumerable.GetEnumerator() =>
        GetEnumerator();

    public ICurve Clone()
    {
        var curves = new List<ICurve>(_curves.Count);
        foreach (var curve in _curves)
            curves.Add(curve.Clone());

        return new Chain(curves);
    }

    private class Enumerator : IEnumerator<ICurve>
    {
        public ICurve Current
        {
            get => _currentNumerator != null ? _currentNumerator.Current : _current;
            private set => _current = value;
        }

        object IEnumerator.Current => Current;

        private int _currentIndex = -1;
        private IEnumerator<ICurve> _currentNumerator;

        private readonly List<ICurve> _curves;
        private ICurve _current;

        public Enumerator(List<ICurve> curves) =>
            _curves = curves;

        public bool MoveNext()
        {
            bool InnerIterator()
            {
                if (_currentNumerator != null)
                {
                    var moveNext = _currentNumerator.MoveNext();
                    if (moveNext)
                        return true;

                    _currentNumerator = null;
                }

                return false;
            }

            void Iterate()
            {
                _currentIndex++;
                if (_currentIndex < _curves.Count)
                {
                    Current = _curves[_currentIndex];
                    _currentNumerator = Current.GetEnumerator();
                    if (_currentNumerator != null && !InnerIterator())
                        Iterate();
                }
            }

            if (InnerIterator()) return true;

            Iterate();

            return _currentIndex < _curves.Count;
        }

        public void Reset() =>
            _currentIndex = 0;

        public void Dispose() =>
            Reset();
    }
}