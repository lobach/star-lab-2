﻿namespace StarLab2.Code.Geometry;

public struct Math
{
    public static double Distance(IPoint a, IPoint b) => 
        System.Math.Sqrt(System.Math.Pow(b.X - a.X, 2) + System.Math.Pow(b.Y - a.Y, 2));
    
    public static double Lerp(double a, double b, double t) => 
        a + (b - a) * System.Math.Clamp(t, 0.0, 1.0);
}