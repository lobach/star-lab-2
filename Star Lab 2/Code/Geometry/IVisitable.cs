﻿namespace StarLab2.Code.Geometry;

public interface IVisitable
{
    public void Accept(IVisitor visitor);
}