﻿namespace StarLab2.Code.Geometry;

public class LengthVisitor : Visitor<double>
{
    private readonly double _t;

    public LengthVisitor(double t) =>
        _t = t;

    protected override double CalcResult(ICurve curve) => 
        curve.CalcLenProcessor(data => data.CurrentStep >= _t).TotalDistance;
}