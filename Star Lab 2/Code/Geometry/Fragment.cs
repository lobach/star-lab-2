﻿using System.Collections;
using System.Collections.Generic;

namespace StarLab2.Code.Geometry;

public class Fragment : ICurve
{
    private readonly ICurve _curve;
    private readonly double _start;
    private readonly double _end;

    public Fragment(ICurve curve, double start, double end)
    {
        _curve = curve;
        _start = start;
        _end = end;
    }

    public void Accept(IVisitor visitor) =>
        visitor.VisitCurve(this);

    public IPoint GetPoint(double t) =>
        _curve.GetPoint(Math.Lerp(_start, _end, t));

    public IEnumerator<ICurve> GetEnumerator() => 
        null;

    IEnumerator IEnumerable.GetEnumerator() => 
        GetEnumerator();

    public ICurve Clone() => 
        new Fragment(_curve.Clone(), _start, _end);
}