﻿namespace StarLab2.Code.Geometry;

public interface IVisitor
{
    public void VisitCurve(ICurve curve);
}

public abstract class Visitor<T> : IVisitor
{
    private T _result;
    
    public T GetResult() => 
        _result;
    
    public void VisitCurve(ICurve curve) => 
        _result = CalcResult(curve);

    protected abstract T CalcResult(ICurve curve);
}