﻿namespace StarLab2.Code.Geometry;

public interface IPoint : ICloneable<IPoint>
{
    public double X { get; set; }
    public double Y { get; set; }
}