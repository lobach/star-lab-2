﻿namespace StarLab2.Code.Visual;

public interface IDrawable
{
    public void Draw();
}