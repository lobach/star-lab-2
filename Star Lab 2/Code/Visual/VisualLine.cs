﻿using StarLab2.Code.Geometry;

namespace StarLab2.Code.Visual;

public class VisualLine : VisualCurve
{
    public VisualLine(IGraphics graphics, ICurve curve)
        : base(graphics, curve)
    {
    }

    public override void Draw() =>
        DrawCurve(Curve, 5);

    protected override VisualCurve CloneSelf(IGraphics graphics) => 
        new VisualLine(graphics, Curve.Clone());
}