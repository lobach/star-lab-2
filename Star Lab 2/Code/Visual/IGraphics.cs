﻿using StarLab2.Code.Geometry;
using StarLab2.Code.UI;

namespace StarLab2.Code.Visual;

public interface IGraphics
{
    public IStyle Style { get; set; }
    public void Clear();
    public void DrawCurve(ICurve curve, int drawSteps);
}