﻿using System.Collections.Generic;
using Avalonia.Collections;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Media;
using StarLab2.Code.Geometry;
using StarLab2.Code.UI;

namespace StarLab2.Code.Visual;

public class AvaloniaContext : IContext
{
    public IStyle Style { get; set; }

    private readonly Canvas _canvas;
    private readonly List<IControl> _spawned;

    private IBrush Brush => new SolidColorBrush(Style.Color);
    private double Thickness => 2;
    private AvaloniaList<double>? Dashes => Style.DrawDashes ? new AvaloniaList<double> { 4, 2 } : null;

    public AvaloniaContext(Canvas canvas)
    {
        _canvas = canvas;
        _spawned = new List<IControl>();
    }

    public void Clear()
    {
        foreach (var entity in _spawned)
            _canvas.Children.Remove(entity);

        _spawned.Clear();
    }

    public void DrawLine(IPoint begin, IPoint end)
    {
        var polyline = new Polyline();
        var points = new List<Avalonia.Point>
        {
            new(begin.X, begin.Y),
            new(end.X, end.Y)
        };
        
        polyline.Points = points;
        polyline.Stroke = Brush;
        polyline.StrokeThickness = Thickness;
        polyline.StrokeDashArray = Dashes;
        AddToCanvas(polyline);
    }

    private void AddToCanvas(IControl control)
    {
        _spawned.Add(control);
        _canvas.Children.Add(control);
    }
}