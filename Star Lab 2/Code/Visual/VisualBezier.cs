﻿using StarLab2.Code.Geometry;

namespace StarLab2.Code.Visual;

public class VisualBezier : VisualCurve
{
    private readonly int _drawSteps;

    public VisualBezier(IGraphics graphics, ICurve curve, int drawSteps)
        : base(graphics, curve) =>
        _drawSteps = drawSteps;

    public override void Draw() =>
        DrawCurve(Curve, _drawSteps);

    protected override VisualCurve CloneSelf(IGraphics graphics) => 
        new VisualBezier(graphics, Curve.Clone(), _drawSteps);
}