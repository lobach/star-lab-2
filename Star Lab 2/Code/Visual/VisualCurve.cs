﻿using System.Collections;
using System.Collections.Generic;
using StarLab2.Code.Geometry;

namespace StarLab2.Code.Visual;

public abstract class VisualCurve : ICurve, IDrawable
{
    public ICurve Curve { get; }

    private readonly IGraphics _graphics;

    protected VisualCurve(IGraphics graphics, ICurve curve)
    {
        Curve = curve;
        _graphics = graphics;
    }

    public IPoint GetPoint(double t) =>
        Curve.GetPoint(t);

    public abstract void Draw();

    public void Accept(IVisitor visitor) =>
        Curve.Accept(visitor);

    protected void DrawCurve(ICurve curve, int drawSteps = 2) =>
        _graphics.DrawCurve(curve, drawSteps);

    public IEnumerator<ICurve> GetEnumerator() => 
        Curve.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => 
        GetEnumerator();

    public ICurve Clone() => 
        CloneSelf(_graphics);

    protected abstract VisualCurve CloneSelf(IGraphics graphics);
}