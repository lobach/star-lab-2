﻿using System.Collections.Generic;
using StarLab2.Code.UI.Memento;
using StarLab2.Code.Visual;

namespace StarLab2.Code;

public interface IModel : ICloneable<Model>
{
    public List<VisualCurve> Curves { get; }
    public void AddCurve(VisualCurve curve);
    public void RemoveCurve(VisualCurve curve);
}