﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class SvgContext : IContext
{
    public IStyle Style { get; set; }

    private Svg _svg;

    public SvgContext() => 
        Clear();

    public void Clear() => 
        _svg = new Svg();

    public void DrawLine(IPoint begin, IPoint end)
    {
        _svg.Curves.Add(new SvgCurve
        {
            Color = Style.Color.ToString(),
            Points = new List<SvgPoint>
            {
                new(begin.X, begin.Y),
                new(end.X, end.Y)
            }
        });
        
        Save();
    }

    private void Save()
    {
        var json = JsonConvert.SerializeObject(_svg);
        var stream = File.CreateText("SVG.json");
        stream.Write(json);
        stream.Close();
    }
}

[Serializable]
public class Svg
{
    public List<SvgCurve> Curves;

    public Svg() => 
        Curves = new List<SvgCurve>();
}

[Serializable]
public class SvgCurve
{
    public string Color;
    public List<SvgPoint> Points;

    public SvgCurve() => 
        Points = new List<SvgPoint>();
}

[Serializable]
public class SvgPoint : IPoint
{
    public double X { get; set; }
    public double Y { get; set; }

    public SvgPoint(double x, double y)
    {
        X = x;
        Y = y;
    }

    public IPoint Clone() => 
        new SvgPoint(X, Y);
}