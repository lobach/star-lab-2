﻿namespace StarLab2.Code.UI.Memento;

public interface IMementoManager
{
    public void Save();
    public void Load();
    public void Registry(IMementable mementable);
    public void UnRegistry(IMementable mementable);
}