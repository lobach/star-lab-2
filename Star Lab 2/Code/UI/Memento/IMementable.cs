﻿namespace StarLab2.Code.UI.Memento;

public interface IMementable
{
    public IMemento CreateMemento();
}