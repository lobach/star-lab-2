﻿using System.Collections.Generic;

namespace StarLab2.Code.UI.Memento;

public class MementoManager : IMementoManager
{
    private HashSet<IMementable> _mementables;
    private HashSet<IMemento>? _lastSave;

    public MementoManager() =>
        _mementables = new HashSet<IMementable>();

    public void Save()
    {
        _lastSave = new HashSet<IMemento>();
        foreach (var m in _mementables)
            _lastSave.Add(m.CreateMemento());
    }

    public void Load()
    {
        if (_lastSave == null) return;

        foreach (var s in _lastSave)
            s.Restore();
    }

    public void Registry(IMementable mementable) =>
        _mementables.Add(mementable);

    public void UnRegistry(IMementable mementable) =>
        _mementables.Remove(mementable);
}