﻿namespace StarLab2.Code.UI.Memento;

public interface IMemento
{
    public void Restore();
}