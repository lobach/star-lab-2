﻿using Avalonia.Media;
using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class GraphicsHolder : IGraphics
{
    private IGraphics _graphics;

    public IStyle Style
    {
        get => _graphics.Style;
        set => _graphics.Style = value;
    }

    public void Clear() =>
        _graphics.Clear();

    public void DrawCurve(ICurve curve, int drawSteps) =>
        _graphics.DrawCurve(curve, drawSteps);

    public void SetContext(IGraphics context) =>
        _graphics = context;
}