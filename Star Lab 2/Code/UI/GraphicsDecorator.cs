﻿using Avalonia.Media;
using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class GraphicsDecorator : IGraphics
{
    public IStyle Style { get; set; }

    private readonly IGraphics _graphics;

    public GraphicsDecorator(IGraphics graphics, IStyle style)
    {
        Style = style;
        _graphics = graphics;
    }

    public void Clear() =>
        _graphics.Clear();

    public void DrawCurve(ICurve curve, int drawSteps)
    {
        _graphics.Style = Style;
        _graphics.DrawCurve(curve, drawSteps);
    }
}