﻿using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class Graphics : IGraphics
{
    public IStyle Style
    {
        get => _context.Style;
        set => _context.Style = value;
    }

    private readonly IContext _context;

    public Graphics(IContext context) =>
        _context = context;

    public void Clear() =>
        _context.Clear();

    public void DrawCurve(ICurve curve, int drawSteps)
    {
        _context.Style = Style;
        var step = 1.0 / (drawSteps - 1);
        for (var i = 1; i < drawSteps; i++)
        {
            var begin = curve.GetPoint(step * (i - 1));
            var end = curve.GetPoint(step * i);
            _context.DrawLine(begin, end);
        }
    }

    /*private void DrawEnds(List<Point> points)
    {
        switch (Style.EndsType)
        {
            case EndsType.Square:
                AddToCanvas(CreateRect(10, 10, points[0].X, points[0].Y));
                AddToCanvas(CreateRect(10, 10, points[^1].X, points[^1].Y));
                break;
            case EndsType.Arrow:
                AddToCanvas(CreateCircle(5, points[0].X, points[0].Y));
                AddToCanvas(CreateTriangle(20, points[^1].X, points[^1].Y));
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private Ellipse CreateCircle(double radius, double x, double y) =>
        new()
        {
            Width = radius * 2,
            Height = radius * 2,
            Margin = new Thickness(0, 0, 0, 0),
            RenderTransform = new TranslateTransform(x - radius, y - radius),
            Stroke = Brush,
            StrokeThickness = Thickness
        };

    private Rectangle CreateRect(double width, double height, double x, double y) =>
        new()
        {
            Width = width,
            Height = height,
            Margin = new Thickness(0, 0, 0, 0),
            RenderTransform = new TranslateTransform(x - width / 2, y - height / 2),
            Stroke = Brush,
            StrokeThickness = Thickness
        };

    private Polygon CreateTriangle(double size, double x, double y)
    {
        var k = Math.Sqrt(3) * size / 4;
        return new Polygon
        {
            Points = new List<Point> { new(x + size / 2, y - k), new(x - size / 2, y - k), new(x, y + k) },
            Margin = new Thickness(0, 0, 0, 0),
            Stroke = Brush,
            StrokeThickness = Thickness
        };
    }*/
}