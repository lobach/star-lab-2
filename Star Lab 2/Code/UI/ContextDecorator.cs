﻿using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class ContextDecorator : IContext
{
    public IStyle Style { get; set; }

    private readonly IContext _context;

    public ContextDecorator(IContext context, IStyle style)
    {
        Style = style;
        _context = context;
    }

    public void Clear() =>
        _context.Clear();

    public void DrawLine(IPoint begin, IPoint end)
    {
        _context.Style = Style;
        _context.DrawLine(begin, end);
    }
}