﻿using StarLab2.Code.Geometry;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI;

public class ContextHolder : IContext
{
    public IStyle Style
    {
        get => Context.Style;
        set => Context.Style = value;
    }

    public IContext? Context { get; set; }

    public void Clear() =>
        Context?.Clear();

    public void DrawLine(IPoint begin, IPoint end) =>
        Context.DrawLine(begin, end);
}