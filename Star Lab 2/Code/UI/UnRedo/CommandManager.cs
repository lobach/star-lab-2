﻿using System.Collections.Generic;
using StarLab2.Code.UI.Commands;
using StarLab2.Code.UI.Memento;

namespace StarLab2.Code.UI.UnRedo;

public class CommandManager : ICommandManager
{
    private const int DumpCommandsCount = 6;

    private readonly IMementoManager _mementoManager;
    private readonly List<ICommand> _commands;

    private bool _lock;

    public CommandManager(IMementoManager mementoManager)
    {
        _mementoManager = mementoManager;
        _commands = new List<ICommand>();
    }

    public void Registry(ICommand command)
    {
        if (_lock) return;

        _commands.Add(command);
        if (_commands.Count > DumpCommandsCount)
            Save(command);
    }

    public void Undo()
    {
        if (_commands.Count <= 1) return;

        _commands.RemoveAt(_commands.Count - 1);
        _lock = true;

        foreach (var command in _commands)
            command.Execute();

        _lock = false;
    }

    private void Save(ICommand command)
    {
        _mementoManager.Save();
        _commands.Clear();
        new RestoreMementoCommand(_mementoManager, this).Execute();
        _commands.Add(command);
    }

    private class RestoreMementoCommand : ACommand
    {
        private readonly IMementoManager _mementoManager;

        public RestoreMementoCommand(IMementoManager mementoManager, ICommandManager commandManager)
            : base(commandManager) =>
            _mementoManager = mementoManager;

        protected override void DoExecute() =>
            _mementoManager.Load();
    }
}