﻿using StarLab2.Code.UI.Commands;

namespace StarLab2.Code.UI.UnRedo;

public interface ICommandManager
{
    public void Registry(ICommand command);
    public void Undo();
}