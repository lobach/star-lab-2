﻿using StarLab2.Code.UI.UnRedo;

namespace StarLab2.Code.UI.Commands;

public abstract class ACommand : ICommand
{
    private readonly ICommandManager _commandManager;

    protected ACommand(ICommandManager commandManager) => 
        _commandManager = commandManager;

    public void Execute()
    {
        _commandManager.Registry(this);
        DoExecute();
    }

    protected abstract void DoExecute();
}