﻿using StarLab2.Code.Geometry;
using StarLab2.Code.UI.UnRedo;
using StarLab2.Code.Visual;

namespace StarLab2.Code.UI.Commands;

public class CreateLineCommand : ACommand
{
    private readonly IPoint _pivot;
    private readonly IPoint _a;
    private readonly IPoint _b;
    private readonly IModelProvider _modelProvider;
    private readonly IGraphics _graphics;

    public CreateLineCommand(IPoint pivot, IPoint a, IPoint b, IModelProvider modelProvider, IGraphics graphics,
        ICommandManager commandManager) : base(commandManager)
    {
        _pivot = pivot;
        _a = a;
        _b = b;
        _modelProvider = modelProvider;
        _graphics = graphics;
    }

    protected override void DoExecute() => 
        _modelProvider.Model.AddCurve(new VisualLine(_graphics, new MoveTo(new Line(_a, _b), _pivot)));
}