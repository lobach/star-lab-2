﻿namespace StarLab2.Code.UI.Commands;

public interface ICommand
{
    public void Execute();
}