﻿using Avalonia.Media;

namespace StarLab2.Code.UI;

public interface IStyle
{
    public Color Color { get; }
    public bool DrawDashes { get; }
    public EndsType EndsType { get; }
}