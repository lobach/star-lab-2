﻿using Avalonia.Media;

namespace StarLab2.Code.UI;

public class StyleData : IStyle
{
    public Color Color { get; }
    public bool DrawDashes { get; }
    public EndsType EndsType { get; }

    public StyleData(Color color, bool drawDashes = false, EndsType endsType = EndsType.Square)
    {
        Color = color;
        DrawDashes = drawDashes;
        EndsType = endsType;
    }
}