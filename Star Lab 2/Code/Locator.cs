﻿namespace StarLab2.Code;

public static class Locator
{
    public static void Set<T>(T instance) => 
        Service<T>.Instance = instance;

    public static T Get<T>() =>
        Service<T>.Instance;

    private static class Service<T>
    {
        public static T Instance;
    }
}