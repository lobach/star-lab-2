﻿using StarLab2.Code.UI.Memento;

namespace StarLab2.Code;

public interface IModelProvider : IMementable
{
    public IModel Model { get; set; }
}