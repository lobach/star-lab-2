using System;
using System.Linq;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Media;
using StarLab2.Code;
using StarLab2.Code.Geometry;
using StarLab2.Code.UI;
using StarLab2.Code.UI.Commands;
using StarLab2.Code.UI.Memento;
using StarLab2.Code.UI.UnRedo;
using StarLab2.Code.Visual;

namespace StarLab2
{
    public partial class MainWindow : Window,
        IModelProvider
    {
        public IModel Model { get; set; }

        private GraphicsHolder _graphics;
        private ContextHolder _context;

        private ICommandManager CommandManager =>
            Locator.Get<ICommandManager>();
        
        private IMementoManager MementoManager =>
            Locator.Get<IMementoManager>();

        public MainWindow()
        {
            InitializeComponent();

            Locator.Set<IModelProvider>(this);
            Locator.Set<IMementoManager>(new MementoManager());
            Locator.Set<ICommandManager>(new CommandManager(MementoManager));

            _context = new ContextHolder();
            _graphics = new GraphicsHolder();
            _graphics.SetContext(new Graphics(_context));

            new StartCommand(this, CommandManager).Execute();
            MementoManager.Registry(this);
            ToBaseContext(null, null);
        }

        private void DrawSample()
        {
            var bezier = CreateBezier();
            var line = CreateLine(bezier.GetPoint(1.0));
            var composite = CreateComposite();

            var count = composite.Count();

            line.Draw();
            bezier.Draw();
            composite.Draw();

            var lenVisitor = new LengthVisitor(0.5);
            bezier.Accept(lenVisitor);

            var stepVisitor = new InverseLengthVisitor(65.67);
            bezier.Accept(stepVisitor);

            var len = lenVisitor.GetResult();
            var step = stepVisitor.GetResult();
        }

        private VisualLine CreateComposite() =>
            new(_graphics, new Chain(
                new Chain(),
                new Chain(),
                new Chain(
                    new MoveTo(new Line(
                        new Point(0, 0),
                        new Point(100, 100)), new Point(200, 200)),
                    new Chain(
                        new MoveTo(new Line(
                            new Point(0, 0),
                            new Point(100, 100)), new Point(200, 200)),
                        new MoveTo(new Line(
                            new Point(100, 100),
                            new Point(150, 0)), new Point(200, 200))
                    )
                ),
                new MoveTo(new Line(
                    new Point(0, 0),
                    new Point(100, 100)), new Point(200, 200)),
                new MoveTo(new Line(
                    new Point(100, 100),
                    new Point(150, 0)), new Point(200, 200))
            ));

        private VisualLine CreateLine(IPoint pivot) =>
            new(
                _graphics,
                new MoveTo(new Line(
                        new Point(0, 0),
                        new Point(100, 100)),
                    pivot));

        private VisualBezier CreateBezier() =>
            new VisualBezier(
                _graphics,
                new Fragment(new Bezier(
                        new Point(0, 0),
                        new Point(30, 50),
                        new Point(50, -30),
                        new Point(100, -50)),
                    1.0,
                    0.0),
                30);

        private void ToBaseContext(object? sender, RoutedEventArgs e) =>
            SetContext(new ContextDecorator(new AvaloniaContext(Canvas),
                new StyleData(Colors.ForestGreen, true, EndsType.Arrow)));

        private void ToCustomContext(object? sender, RoutedEventArgs e) =>
            SetContext(new ContextDecorator(new AvaloniaContext(Canvas),
                new StyleData(Colors.Black)));

        private void SetContext(IContext context)
        {
            _context.Clear();
            _context.Context = context;
            //DrawSample();
        }

        private void ExportToSvg(object? sender, RoutedEventArgs e)
        {
            var context = _context.Context;
            SetContext(new ContextDecorator(new SvgContext(), context.Style));
            //DrawSample();
            SetContext(context);
        }

        private void GenerateCurves(object? sender, RoutedEventArgs e)
        {
            //DrawSample();
        }

        private void CanvasPress(object? sender, PointerReleasedEventArgs e)
        {
            var position = e.GetPosition(Canvas);
            new CreateLineCommand(new Point(position.X, position.Y), new Point(0, 0), new Point(10, 10), this,
                _graphics, CommandManager).Execute();
            DrawModel();
        }

        private void DrawModel()
        {
            _context.Clear();
            foreach (var line in Model.Curves)
                line.Draw();
        }

        private void Undo(object? sender, RoutedEventArgs e)
        {
            CommandManager.Undo();
            DrawModel();
        }

        public IMemento CreateMemento() => 
            new Memento(this);

        private class StartCommand : ACommand
        {
            private readonly IModelProvider _modelProvider;

            public StartCommand(IModelProvider modelProvider, ICommandManager commandManager) : base(commandManager)
            {
                _modelProvider = modelProvider;
            }

            protected override void DoExecute() =>
                _modelProvider.Model = new Model();
        }

        private class Memento : IMemento
        {
            private readonly IModelProvider _modelProvider;
            private readonly Model _dump;

            public Memento(IModelProvider modelProvider)
            {
                _modelProvider = modelProvider;
                _dump = _modelProvider.Model.Clone();
            }

            public void Restore() => 
                _modelProvider.Model = _dump.Clone();
        }
    }
}